// SPDX-License-Identifier: MIT
pragma solidity ^0.8.22;

import "@openzeppelin/contracts/access/Ownable.sol";

contract Voting is Ownable {

    enum WorkflowStatus {
        RegisteringVoters,
        ProposalsRegistrationStarted,
        ProposalsRegistrationEnded,
        VotingSessionStarted,
        VotingSessionEnded,
        VotesCount
    }

    struct Voter {
        bool isRegistered;
        bool hasVoted;
        uint votedProposalId;
    }

    struct Proposal {
        string description;
        uint voteCount;
    }

    Proposal[] public proposals;
    mapping(address => Voter) public voters;
    WorkflowStatus public workflowStatus;
    uint public winningProposalId;

    constructor(address _initialOwner) Ownable(_initialOwner) {
        workflowStatus = WorkflowStatus.RegisteringVoters;
    }

    modifier onlyAtStatus(WorkflowStatus _status) {
        require(workflowStatus == _status, "Invalid workflow status");
        _;
    }

    event VoterRegistered(address voterAddress);
    event WorkflowStatusChange(WorkflowStatus previousStatus, WorkflowStatus newStatus);
    event ProposalRegistered(uint proposalId);
    event Voted(address voter, uint proposalId);

    function startProposalsRegistration() public onlyOwner onlyAtStatus(WorkflowStatus.RegisteringVoters) {
        workflowStatus = WorkflowStatus.ProposalsRegistrationStarted;
        emit WorkflowStatusChange(WorkflowStatus.RegisteringVoters, workflowStatus);
    }

    function endProposalsRegistration() public onlyOwner onlyAtStatus(WorkflowStatus.ProposalsRegistrationStarted) {
        workflowStatus = WorkflowStatus.ProposalsRegistrationEnded;
        emit WorkflowStatusChange(WorkflowStatus.ProposalsRegistrationStarted, workflowStatus);
    }

    function startVotingSession() public onlyOwner onlyAtStatus(WorkflowStatus.ProposalsRegistrationEnded) {
        workflowStatus = WorkflowStatus.VotingSessionStarted;
        emit WorkflowStatusChange(WorkflowStatus.ProposalsRegistrationEnded, workflowStatus);
    }

    function endVotingSession() public onlyOwner onlyAtStatus(WorkflowStatus.VotingSessionStarted) {
        workflowStatus = WorkflowStatus.VotingSessionEnded;
        emit WorkflowStatusChange(WorkflowStatus.VotingSessionStarted, workflowStatus);
    }

    function registerVoter(address _voterAddress) public onlyOwner onlyAtStatus(WorkflowStatus.RegisteringVoters) {
        require(!voters[_voterAddress].isRegistered, "Voter is already registered");
        voters[_voterAddress] = Voter({
            isRegistered: true,
            hasVoted: false,
            votedProposalId: 0
        });

        emit VoterRegistered(_voterAddress);
    }

    function registerProposal(string memory _description) public onlyAtStatus(WorkflowStatus.ProposalsRegistrationStarted) {
        require(voters[msg.sender].isRegistered, "Only registered voters can submit proposals.");
        proposals.push(Proposal({
            description: _description,
            voteCount: 0
        }));
        emit ProposalRegistered(proposals.length - 1);
    }

    // For dapp
    function getProposalsCount() public view returns(uint) {
        return proposals.length;
    }

    // For dapp
    function getProposal(uint index) public view returns (string memory, uint) {
        require(index < proposals.length, "Index out of range");
        return (proposals[index].description, proposals[index].voteCount);
    }

    function vote(uint _proposalId) public onlyAtStatus(WorkflowStatus.VotingSessionStarted) {
        require(voters[msg.sender].isRegistered, "Voter is not registered");
        require(!voters[msg.sender].hasVoted, "Voter has already voted");
        require(_proposalId < proposals.length, "Invalid proposal ID");

        voters[msg.sender].hasVoted = true;
        voters[msg.sender].votedProposalId = _proposalId;
        proposals[_proposalId].voteCount++;

        emit Voted(msg.sender, _proposalId);
    }

    function getWinner() public view onlyAtStatus(WorkflowStatus.VotesCount) returns (uint) {
        return winningProposalId;
    }

    function countVotes() public onlyOwner onlyAtStatus(WorkflowStatus.VotingSessionEnded) {
        workflowStatus = WorkflowStatus.VotesCount;
        emit WorkflowStatusChange(WorkflowStatus.VotingSessionEnded, workflowStatus);
        uint maxVoteCount = 0;
        for (uint i = 0; i < proposals.length; i++) {
            if (proposals[i].voteCount > maxVoteCount) {
                maxVoteCount = proposals[i].voteCount;
                winningProposalId = i;
            }
        }
    }

    // For dapp
    function getWorkflowStatus() public view returns(uint16) {
        return uint16(workflowStatus);
    }

    // For dapp
    function isRegister() public view returns(bool) {
        return voters[msg.sender].isRegistered;
    }

    // Fonctionnalité permettant de savoir le nombre de votes sur une proposition
    function auditVotes(uint _proposalId) public onlyAtStatus(WorkflowStatus.VotingSessionStarted) view returns(uint) {
        require(_proposalId < proposals.length, "Invalid proposal ID");
        return proposals[_proposalId].voteCount;
    }

    // Permet à un utilisateur de vérifier s'il a déjà voté et de savoir pour quelle proposition
    function verifyVote() public view returns (bool, uint) {
        require(voters[msg.sender].isRegistered, "Voter is not registered");
        return (voters[msg.sender].hasVoted, voters[msg.sender].votedProposalId);
    }
    
    // For dapp
    function getTotalVotes() public view returns(uint) {
        uint res = 0;
        for(uint i = 0; i<proposals.length; i++) {
            res = res + proposals[i].voteCount;
        }
        return res;
    }

    // For dapp
    function getVoter(address _voterAddress) public view returns(bool, bool, uint) {
        Voter storage voter = voters[_voterAddress];
        return (voter.isRegistered, voter.hasVoted, voter.votedProposalId);
    }
}