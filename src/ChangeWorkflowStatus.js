import './App.css';

export default function ChangeWorkflowStatus({contract, workflow, updateWorkflowStatus, updateWinner}) {
    
    async function startProposalsRegistration() {
        try {
            const owner = await contract.methods.owner().call();
            await contract.methods.startProposalsRegistration().send({from: owner});
            updateWorkflowStatus();
        } catch(error) {
            console.log(error)
        }
    }

    async function endProposalsRegistration() {
        try {
            const owner = await contract.methods.owner().call();
            await contract.methods.endProposalsRegistration().send({from: owner});
            updateWorkflowStatus();
        } catch(error) {
            console.log(error)
        }
    }

    async function startVotingSession() {
        try {
            const owner = await contract.methods.owner().call();
            await contract.methods.startVotingSession().send({from: owner});
            updateWorkflowStatus();
        } catch(error) {
            console.log(error)
        }
    }

    async function endVotingSession() {
        try {
            const owner = await contract.methods.owner().call();
            await contract.methods.endVotingSession().send({from: owner});
            updateWorkflowStatus();
        } catch(error) {
            console.log(error)
        }
    }

    async function countVotes() {
        try {
            const owner = await contract.methods.owner().call();
            await contract.methods.countVotes().send({from: owner});
            updateWorkflowStatus();
            updateWinner();
        } catch(error) {
            console.log(error)
        }
    }

    return (
        <div>
            {workflow === 0 && <button className="workflow-button" onClick={startProposalsRegistration}>Start proposals registrations</button>}
            {workflow === 1 && <button className="workflow-button" onClick={endProposalsRegistration}>End proposals registrations</button>}
            {workflow === 2 && <button className="workflow-button" onClick={startVotingSession}>Start voting session</button>}
            {workflow === 3 && <button className="workflow-button" onClick={endVotingSession}>End voting session</button>}
            {workflow === 4 && <button className="workflow-button" onClick={countVotes}>Count votes</button>}
            {workflow === 5 && <p>Vote is finished</p>}
        </div>);
}