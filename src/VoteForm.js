import { useState } from 'react';
import './App.css';

export default function VoteForm({account, contract, voted, updateVoted, proposal}) {
    const [proposalID, setProposalID] = useState(0);
    const [message, setMessage] = useState('');

    function handleVote(e) {
        setProposalID(e.target.value);
    }

    async function vote(e) {
        e.preventDefault();
        try {
            await contract.methods.vote(proposalID).send({from: account});
            setMessage('You have voted successfully !');
            updateVoted();
        } catch(error) {
            setMessage('Enter an ID among those of the proposals.')
        }
    }

    return(
    <div>
        {!voted ? (
            <form className="vote" onSubmit={vote}>
            <input className="vote-input" placeholder="Enter the proposal ID for your vote" value={proposalID} onChange={handleVote}></input>
            <button className="vote-button" type='submit'>Vote</button>
        </form>
        ): (<p>Your vote : {proposal}</p>)
        }
        {message && <p className='error'>{message}</p>}
    </div>
    );
}