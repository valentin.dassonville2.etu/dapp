import { useEffect, useState } from 'react';
import './App.css';
import Web3 from 'web3';
import Voting from "./build/Voting.json"
import { validator } from 'web3-validator';
import AddVoterForm from './AddVoterForm';
import ChangeWorkflowStatus from './ChangeWorkflowStatus';
import AddProposalForm from './AddProposalForm';
import ListProposals from './ListProposals';
import VoteForm from './VoteForm';

export const WorflowStatus = {
  0: "Registering Voters",
  1: "Proposals Registration Started",
  2: "Proposals Registration Ended",
  3: "Voting Session Started",
  4: "Voting Session Ended",
  5: "Vote Count"
}

// Mettre l'adresse du contrat ici
const contractAddress = "0x3Ad438090D6CA3c26f2e4C4c2E7833066B87e709";

const web3 = new Web3("http://localhost:8545");
const contract = new web3.eth.Contract(Voting.abi, contractAddress);

export default function App() {
  const [accounts, setAccounts] = useState([]);
  const [account, setAccount] = useState('');
  const [isOwner, setIsOwner] = useState(false);
  const [currentWorkflow, setCurrentWorkflow] = useState(0);
  const [owner, setOwner] = useState('');
  const [isRegister, setIsRegister] = useState(false);
  const [proposals, setProposals] = useState([]);
  const [hasVoted, setHasVoted] = useState(false);
  const [proposalVote, setProposalVote] = useState(0);
  const [totalVotes, setTotalVotes] = useState(0);
  const [winner, setWinner] = useState({});

  useEffect(() => {
    async function init() {
      const owner = await contract.methods.owner().call();
      setOwner(owner);
      const accs = await web3.eth.getAccounts();
      setAccounts(accs);
      setAccount(accs[0]);
    }
    init();
  }, []);

  useEffect(() => {
    async function update() {
      let register = await contract.methods.isRegister().call({from: account});
      if(account === owner) {
        setIsOwner(true);
        if(!register) {
          try {
            validator.validate(['address'], [owner]);
            await contract.methods.registerVoter(owner).send({from: owner});
            register = true;
          } catch(error) {}
        }
      } else {
        setIsOwner(false);
      }
      setIsRegister(register);
      updateWorkflowStatus();
      updateProposals();
      if(register) updateHasVoted();
      getTotalVotes();
    }

    if(account !== '') update();
  }, [account]);

  async function updateWorkflowStatus() {
    const workflow = await contract.methods.getWorkflowStatus().call();
    setCurrentWorkflow(Number(workflow));
  }

  async function updateProposals() {
    try {
        const tmp = [];
        const proposalsCount = await contract.methods.getProposalsCount().call();
        for(let i=0; i<proposalsCount; i++) {
            const proposal = await contract.methods.getProposal(i).call();
            tmp.push({
                id: i,
                description: proposal[0]
            });
        }
        setProposals(tmp);
    } catch(error) {
        console.log("Error has occured during updateProposals :\n", error);
    }
  }

  async function updateHasVoted() {
    try {
      const voted = await contract.methods.verifyVote().call({from: account});
      setHasVoted(voted[0]);
      setProposalVote(Number(voted[1]));
      getTotalVotes();
    } catch(error) {
      console.log(error);
    }
  }

  async function getTotalVotes() {
    try {
      const total = await contract.methods.getTotalVotes().call({from: account});
      setTotalVotes(Number(total));
    } catch(error) {
      console.log(error);
    }
  }

  async function getWinner() {
    try {
      const win = await contract.methods.getWinner().call({from: account});
      const proposalWinner = await contract.methods.getProposal(win).call({from: account});
      setWinner({
        id: Number(win),
        description: proposalWinner[0],
        voteCount: Number(proposalWinner[1])
      });
    } catch(error) {
      console.log(error);
    }
  }

  function handleSelect(e) {
    setAccount(e.target.value)
  }

  return (
    <div className="App">
      <header className="App-header">
        <h1>Voting System</h1>
        <h3>Current step : {WorflowStatus[currentWorkflow]}</h3>
        <h6>Contract owner : {owner}</h6>
        <div>
          <p>Connected account : 
            <select onChange={handleSelect}>
            {
              accounts.map((acc, index) => (
                <option key={index} value={acc}>{acc}</option>
              ))
            }
            </select></p>
        </div>
        <div className='mainPanel'>
          <h2>Main panel</h2>
          { isRegister ? (
            <div>
              {currentWorkflow >= 1 && currentWorkflow <= 4 && <ListProposals proposals={proposals}></ListProposals>}
              {currentWorkflow === 0 && <p>You are registered. Waiting proposals registration to start.</p>}
              {currentWorkflow === 1 && <AddProposalForm account={account} contract={contract} updateProposals={updateProposals}></AddProposalForm>}
              {currentWorkflow === 2 && <p>Proposals registration is finished. Waiting to start votes.</p>}
              {currentWorkflow === 3 && <p>Number of votes counted : {totalVotes}</p>}
              {currentWorkflow === 3 && <VoteForm account={account} contract={contract} voted={hasVoted} updateVoted={updateHasVoted} proposal={proposalVote}></VoteForm>}
              {currentWorkflow === 4 && <p>Voting session is finished. Waiting to count votes.</p>}
              {currentWorkflow === 5 && (
                <div>
                  <p>Winner proposal : {winner.id}</p>
                  <p>Description : {winner.description}</p>
                  <p>Vote count : {winner.voteCount}</p>
                </div>              
              )
            }
            </div>
          ) : 
          (<p>You are not registered</p>)
          }
        </div>
        {
          isOwner && (
            <div className='adminPanel'>
              <h2>Admin panel</h2>
              {currentWorkflow === 0 && <AddVoterForm contract={contract}></AddVoterForm>}
              <ChangeWorkflowStatus contract={contract} 
              workflow={currentWorkflow} 
              updateWorkflowStatus={updateWorkflowStatus} 
              updateWinner={getWinner} 
              ></ChangeWorkflowStatus>
            </div>
          )
        }
      </header>
    </div>
  );
}