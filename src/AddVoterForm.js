import { useState } from "react";
import { validator } from 'web3-validator';
import './App.css';

export default function AddVoterForm({contract}) {
    const [voterAddress, setVoterAddress] = useState('');
    const [message, setMessage] = useState('');

    function handleAddressChange(e) {
        setVoterAddress(e.target.value);
    }

    async function registerVoter(e) {
        e.preventDefault();
        try {
            const owner = await contract.methods.owner().call();
            validator.validate(['address'], [voterAddress]);
            await contract.methods.registerVoter(voterAddress).send({from: owner});
            setMessage('Voter correctly added !');
            setVoterAddress('');
        } catch (error) {
            setMessage("Error when trying to add Voter. The voter is already added or the address isn't correct.");
        }
    }
    
    return (
        <div>
            <h3>Register Voter</h3>
            <form className="add-voter" onSubmit={registerVoter}>
                <input className="add-voter-input" type='text' placeholder="Address of the voter to add" value={voterAddress} onChange={handleAddressChange}></input>
                <button className="add-voter-button" type='submit'>Add</button>
            </form>
            {message && <p className="error">{message}</p>}
        </div>);
}