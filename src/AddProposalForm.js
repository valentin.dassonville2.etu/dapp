import { useState } from 'react';
import './App.css';

export default function AddProposalForm({account, contract, updateProposals}) {
    const [proposal, setProposal] = useState('');
    const [message, setMessage] = useState('');

    function handleProposal(e) {
        setProposal(e.target.value);
    }

    async function registerProposal(e) {
        e.preventDefault();
        try {
            await contract.methods.registerProposal(proposal).send({from: account});
            setMessage('Proposal registered successfully !');
            updateProposals();
            setProposal('');
        } catch(error) {
            setMessage('An error has occured.')
        }
    }

    return(
    <div>
        <form className="add-proposal" onSubmit={registerProposal}>
            <textarea className="add-proposal-input" placeholder="Enter proposal description" value={proposal} onChange={handleProposal}></textarea>
            <button className="add-proposal-button" type='submit'>Add</button>
        </form>
        {message && <p className='error'>{message}</p>}
    </div>
    );
}