import './App.css';

export default function ListProposals({proposals}) {
    return(
        <div>
            <h3>Proposals</h3>
            <table className='proposals'>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        proposals.map(proposal => (
                            <tr key={proposal.id}>
                                <td>{proposal.id}</td>
                                <td>{proposal.description}</td>
                            </tr>
                        ))
                    }
                </tbody>
            </table>
        </div>
    );
}