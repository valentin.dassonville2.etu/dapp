# Dapp Voting

Ce projet a été réalisé par Valentin Dassonville

# Prérequis

Installer truffle et ganache si ils ne sont déjà pas installer : `npm install -g truffle` et `npm install -g ganache`

Faire `npm i` pour installer les dépendances.

Compiler le smartcontract : `truffle compile`

Lancer la blockchain local : `npm run ganache`  
Utiliser bien ce raccourci car il défini des paramètres d'exécution pour ganache.  
Cette commande utilisera la chaîne stocké dans le dossier ganache.

Cette étape est nécessaire si vous utilisez une autre blockchain que celle de développement sur le projet.  
Modifier dans **migrations/2_Voting.js** l'adresse de l'owner du contrat (sinon ce sera le premier compte sur ganache par défaut).  
Déployer le contrat : `truffle migrate`  
Garder l'adresse du contrat (correspond à la ligne "Contract created" sur ganache).  
Dans **src/App.js**, mettre l'adresse du contrat dans la variable **contractAddress** qui se trouve au début du fichier.

# Lancer l'application react

Executer : `npm start`

L'application est disponible sur l'url `localhost:3000`

# Remarques

Le contrat solidity est disponible ici : [Voting.sol](./contracts/Voting.sol)

Démo de la dapp : voir [`demo_dapp.mp4`](./demo_dapp.mp4)

Les comptes ne sont pas reliés à un outil comme MetaMask, je n'ai pas eu le temps.

Je n'ai pas réussi à le déployer sur un outil comme Heroku.